## Prepare project structure
1. mkdir <project name>
1. mkdir <project name>/server
1. mkdir <project name>/client

## Prepare git
1. git init
1. create a git ignore file .gitignore eg(node_modules/specify directory_level)
1. git remote -v (check on the current directory which remote is configured to)
1. git remove origin , git remove origin
1. add remote origin , git remote add origin < http git url> . eg git remote add origin https://yztan@bitbucket.org/yztan/day3.git

##Global utility installation
1. npm install -g nodemon
1. npm install -g bower

## Prepare express JS
1. npm init
1. npm install express --save

## Create an app.js in the server directory
1. cd <project name> <server folder>
1. touch app.js

## How to start my app
1. export NODE_PORT=3000
1. nodemon or nodemon server/app.js

## How to test my app
1. Launch Chrome, point to http://localhost3000

## Launch code at visual studio from git bash
1. cd <project name>
1. code . 

## Commit changes and push to repo master/branch
1. git add . 
1. git commit -m "<message>"
1. git push origin master/branch

## Remove files from folder
1. In the project directory, type git rm <file name>

## Create branch 
1. git checkout -b <branch name>

## Switch back to master
1. git checkout -b <master>

## Cloning other user's folder
1. mkdir <project folder> (to create a folder to keep other people's code)
1. cd <project folder>
1. git clone <http git url>


 